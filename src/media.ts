
interface MediaType {
  width: number,
  height: number,
  type: string
}

const mediaTypes: { [key: string]: MediaType } = {
  largeRectangle: {
    width: 336,
    height: 280,
    type: 'image'
  },
  mediumRectangle: {
    width: 300,
    height: 250,
    type: 'image'
  },
  leaderBoard: {
    width: 728,
    height: 90,
    type: 'image'
  },
  skyscraper: {
    width: 300,
    height: 600,
    type: 'image'
  },
  mobileLeaderBoard: {
    width: 320,
    height: 50,
    type: 'image'
  },
  video: {
    width: 1280,
    height: 720,
    type: 'video'
  }
}

export default mediaTypes