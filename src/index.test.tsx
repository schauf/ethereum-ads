import React from 'react'
// tslint:disable-next-line: no-implicit-dependencies
import { create } from 'react-test-renderer'

import EthereumAds from './ethereumAds'

describe('EthereumAds component banner', () => {
  test('it render banner', () => {
    const props = {
      address: '0x4E56AD43B1Bf7b5D351A41827a39B39FEc64Cf70',
      slot: 1,
      type: 'largeRectangle'
    }
    const component = create(<EthereumAds  {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })

  test('it renders video', () => {
    const props = {
      address: '0x4E56AD43B1Bf7b5D351A41827a39B39FEc64Cf70',
      slot: 1,
      type: 'video'
    }
    const component = create(<EthereumAds  {...props} />)
    expect(component.toJSON()).toMatchSnapshot()
  })
})