import React, { memo } from 'react'
import mediaTypes from './media'

interface EthereumAdsProps {
  address: string
  slot: number
  type: string
}

const EthereaumAds: React.FC<EthereumAdsProps> = ({ address, slot, type }) => {
  const selectedMedia = mediaTypes[type] || mediaTypes['largeRectangle']

  const query = `?address=${address}&slot=${slot}&width=${selectedMedia.width}&height=${selectedMedia.height}`
  const adLink = 'https://ethereumads.com/link' + query
  const mediaLink = 'https://ethereumads.com/media' + query
  const text = 'Earn cryptocurrency with banner ads'

  if (selectedMedia.type === 'image') {

    const containerStyles: React.CSSProperties = {
      position: 'relative',
      overflow: 'hidden',
      margin: 'auto',
      ...selectedMedia
    }

    const imageStyles: React.CSSProperties = {
      position: 'absolute',
      top: 0,
      left: 0,
      margin: 0
    }

    return (
      <div style={containerStyles}>
        <a href="https://ethereumads.com" target="_new">{text}</a>
        <a href={adLink}>
          <img  alt={text} src={mediaLink}  style={imageStyles} />
        </a>
      </div>
    )
  }

  if (selectedMedia.type === 'video') {
    return (
      <a href={adLink}>
        <video width={selectedMedia.width} height={selectedMedia.height} autoPlay={true}>
          <source src={mediaLink} type="video/mp4" />
        </video>
      </a>
    )
  }

  return null
}

export default memo(EthereaumAds)