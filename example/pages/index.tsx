import Head from 'next/head'
import { useRouter } from 'next/router'
import Link from 'next/link'

import EthereumAds from 'ethereum-ads'
import mediaTypes from 'ethereum-ads/dist/media'

export default function Home() {
  const router = useRouter()
  const { queryType } = router.query
  const defaultType = mediaTypes['mediumRectangle'].type

  const getTypeMenu = () => {
    return Object.keys(mediaTypes).map(type => (
      <li key={type}>
        <Link href={`/?queryType=${type}`}>
          {type}
        </Link>
      </li>
    ))
  }

  const props = {
    address: '0x4E56AD43B1Bf7b5D351A41827a39B39FEc64Cf70',
    slot: 1,
    type: defaultType
  }

  return (
    <div className="container">
      <Head>
        <title>EthereumAds - ReactJS Component</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>EthereumAds</h1>
        <h4>Types of Advertisements</h4>

        <ul>
          {getTypeMenu()}
        </ul>

        <section>
          <EthereumAds {...props} />
        </section>

      </main>
    </div>
  )
}
