# EthereumAds

**Table of Content**
- [Introducing](#introducing)
- [Get Started](#get-started)
- [Develop](#develop)
- [Donate](#donate)
- [License](#license)

**Dependencies**
- ReactJS

## Introducing
This package provides a `ReactJS Component` to render image or video `advertisment banners` that are provided by [EthereumAds](https://ethereumads.com).

This type of advertisment is completely anonymous, because you get payed off in Ethereum (ETH).

### Why EthereumAds?

#### Perfect targeting
The Promise is to get a better and competitive perfect targeting of your audience.

#### Lower commissions
Google Adsense only pays you `68%` of your ad earnings. 
EthereumAds on the other hand pay you a whopping `90%`.

#### More about
visit [EthereumAds](https://ethereumads.com)

### How it Works

After you use this package, ad space is automatically openly auctioned off using smart contracts every two weeks to the highest bidder. This service is suitable for any content, not just crypto-related, e.g. websites, blogs, videos, even billboards. Just let the open bidding market automatically and continually find the perfect ads for you. The earnings go directly into your Ethereum account.

## Get Started

First you must install this package via Packagemanager as follow for npm
`npm install ethereum-ads` or yarn `yarn add ethereum-ads`.

than import this package via ES6 `import`

```jsx
import EthereumAds from 'ethereum-ads'
```

or use the CommonJS `require` statement

```jsx
const  EthereumAds = require('ethereum-ads')
```

now add your personal settings:

```jsx
import EthereumAds from 'ethereum-ads'

const YourComponent = ({title}) => {

  const AdSettings = {
    slot: 0,
    type: 'largeRectangle',
    address: '0x4E56AD43B1Bf7b5D351A41827a39B39FEc64Cf70'
  }

  return (
    <aside>
      <Headline>{title}</Headline>
      <EthereumAds {...AdSettings} />
    </aside>
  )
}
```

### Properties

#### address (required)
Your ethereum wallet public key also known as your wallet address. 
You don't have a wallet?
Generate one here [Metamask](https://metamask.io/).

#### slot (required)
If you use this components more than once, 
you have to manage different slot numbers to identify each.
Otherwise leave it 0.

#### type  (required)
Set name as type value to select one of following formats:

| Name              | Type   | Width | Height |
| ----------------- |:------:| -----:|-------:|
| largeRectangle    | Image  | 336   | 280    |
| mediumRectangle   | Image  | 300   | 250    |
| leaderBoard       | Image  | 728   | 90     |
| skyscraper        | Image  | 300   | 600    |
| mobileLeaderBoard | Image  | 320   | 50     |
| video             | Video  | 1280  | 720    |

select a type of ads and declare it like the follow code snippet:

```jsx
<EthereumAds type="mobileLeaderBoard />
```

---

## Develop

### Command Overview
| Command        | Description   |
| :------------- |:------|
| yarn install   | Install dependencies |
| yarn dev       | Run watcher for typescript compiler |
| yarn tests     | Run tests |
| yarn build     | build production version |

### Run Example 
Check Component in Example
```
cd example
yarn install
yarn dev
````

## Deployment

increase Package version
```
yarn version --pre --major    
1.0.0 -> 2.0.0

yarn version --pre --minor
1.0.0 -> 1.1.0

yarn version --pre --patch
1.0.0 -> 1.0.1

yarn version --pre
1.0.0-alpha.0 -> 1.0.0-alpha.1
```

add all changes
```
git add .
git commit -m 'Commit Message'
git push
```

Auto Deploy will release the new Version on [NPM Registry](https://www.npmjs.com/package/ethereum-ads)

## Donate

Ethereum: [0x4E56AD43B1Bf7b5D351A41827a39B39FEc64Cf70](https://etherscan.io/address/0x4E56AD43B1Bf7b5D351A41827a39B39FEc64Cf70)

![Ethereum](./assets/ethereum.png)

Bitcoin: [bc1qzhrznfn08rvh9mt4yy2x4zd6crxlzpnldawtkq](https://www.blockchain.com/btc/address/bc1qzhrznfn08rvh9mt4yy2x4zd6crxlzpnldawtkq)

![Bitcoin](./assets/bitcoin.png)

## License
ISC License

Copyright (c) 2020, Holger D. Schauf

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.